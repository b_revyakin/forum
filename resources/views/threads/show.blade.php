@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
            <a href="#">{{ $thread->creator->name }}</a> posted:
            {{ $thread->title  }}
          </div>

          <div class="panel-body">
            @if (session('status'))
              <div class="alert alert-success">
                {{ session('status') }}
              </div>
            @endif

            {{ $thread->body }}
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        @foreach($thread->replies as $reply)
          <div class="panel panel-default">
            <div class="panel-heading">
              <a href="#">{{ $reply->owner->name }}</a> said {{ $reply->created_at->diffForHumans() }}...
            </div>
            <div class="panel-body">
              {{ $reply->body }}
            </div>
          </div>
        @endforeach
      </div>
    </div>
    @if(auth()->check())
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <form method="POST" action="{{ $thread->path() . '/replies' }}">
            {!! csrf_field() !!}

            <div class="form-group">
              <label for="body">Body:</label>
              <textarea id="body" name="body" class="form-control"></textarea>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-default">Post</button>
            </div>
          </form>
        </div>
      </div>
    @else
      <p class="text-center">Please <a href="{{ route('login') }}">sign in</a> for post</p>
    @endif
  </div>
@endsection
